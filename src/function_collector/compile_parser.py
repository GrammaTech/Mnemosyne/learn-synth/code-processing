from tree_sitter import Language


def main():
    Language.build_library(
        # Store the library in the `build` directory
        "build/tree-sitter-python.so",
        # Include one or more languages
        ["tree-sitter-python"],
    )


if __name__ == "__main__":
    main()
