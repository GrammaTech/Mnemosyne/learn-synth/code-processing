import argparse
import logging
import os
import sys
from collections import Counter
from dataclasses import dataclass, field
from pathlib import Path

from tree_sitter import Language, Parser

PY_LANGUAGE = Language("build/tree-sitter-python.so", "python")


def get_python_parser():
    """
    Get a python parser object
    """
    parser = Parser()
    parser.set_language(PY_LANGUAGE)
    return parser


@dataclass
class FunctionStats:
    """
    Function call statistics
    """

    files: int = 0
    error_files: int = 0
    LOC: int = 0
    fun_defs: int = 0
    fun_call: int = 0
    n_args: Counter = field(default_factory=Counter)
    arg_type: Counter = field(default_factory=Counter)
    identifier_arg_parts: Counter = field(default_factory=Counter)

    def print_stats(self):
        print(f"Number of files correctly parsed: {self.files}")
        print(f"Number of files with parse errors: {self.error_files}")
        print(f"LOC: {self.LOC}")
        print(f"Number of function definitions: {self.fun_defs}")
        print(f"Number of function calls: {self.fun_call}")
        print("Number of arguments stats:")
        for elem, frequency in self.n_args.most_common():
            print(
                f"\t{elem} args: {frequency} ({frequency/self.fun_call:.2%})"
            )
        print("Argument type stats:")
        total_args = sum(self.arg_type.values())
        for type, frequency in self.arg_type.most_common():
            print(f"\t{type} args: {frequency} ({frequency/total_args:.2%})")
        print("Indentifier parts:")
        total_identifier_args = sum(self.identifier_arg_parts.values())
        for num_parts, frequency in self.identifier_arg_parts.most_common():
            print(
                f"\t{num_parts} parts: {frequency} "
                f"({frequency/total_identifier_args:.2%})"
            )


class ParsedFile:
    """
    This class represent a parsed python file
    """

    def __init__(self, parser: Parser, filename: Path):
        with open(filename) as f:
            text = f.read()
            tree = parser.parse(bytes(text, "utf8"))
        self.text = text
        self.tree = tree

    def get_node_text(self, node):
        """
        Get the text corresponding to a parse tree node.
        """
        return self.text[node.start_byte : node.end_byte]


    def get_num_function_defs(self) -> int:
        """
        Compute the number of function definitions
        """
        query = PY_LANGUAGE.query("(function_definition) @fun_def")
        captured = query.captures(self.tree.root_node)
        return len(captured)

    def get_argument_type(self, argument) -> str:
        if argument.type == "keyword_argument":
            argument = argument.child_by_field_name("value")
        return argument.type

    def get_function_calls(self, stats: FunctionStats):
        """
        Collect function call statistics.
        """
        query = PY_LANGUAGE.query("(call) @fun_call")
        captured = query.captures(self.tree.root_node)
        for captured_node, _ in captured:
            # name_node = captured_node.child_by_field_name("name")
            arguments_node = captured_node.child_by_field_name("arguments")
            arguments = [
                child
                for child in arguments_node.children
                if child.is_named and child.type != "comment"
            ]
            argument_types = [
                self.get_argument_type(argument) for argument in arguments
            ]
            stats.fun_call += 1
            stats.n_args[len(arguments)] += 1
            stats.arg_type.update(argument_types)

            for node in arguments:
                if node.type == "keyword_argument":
                    node = node.child_by_field_name("value")
                if node.type == "identifier":
                    argument_text = self.get_node_text(node)
                    n_parts = len(
                        [
                            part
                            for part in argument_text.split("_")
                            if part != ""
                        ]
                    )
                    stats.identifier_arg_parts[n_parts] += 1


def process_file(parser: Parser, filename: Path, stats: FunctionStats) -> None:
    """
    Collect function call statistics on a single python file.
    """
    parsed_file = ParsedFile(parser, filename)
    if parsed_file.tree.root_node.has_error:
        logging.warning("File has parsing errors, ignoring")
        stats.error_files += 1
        return
    stats.files += 1
    stats.LOC += len(parsed_file.text.split("\n"))
    stats.fun_defs += parsed_file.get_num_function_defs()
    parsed_file.get_function_calls(stats)


def process_directory(
    parser: Parser, directory: Path, stats: FunctionStats
) -> None:
    """
    Collect statistics on all python files in a directory.
    """
    for dirpath, _, files in os.walk(directory):
        for file in files:
            file_path = Path(dirpath) / file
            if file.endswith(".py"):

                logging.info(f"Processing {file_path}")
                try:
                    process_file(parser, file_path, stats)
                except UnicodeDecodeError:
                    logging.warning(f"Processing {file} failed")
                except FileNotFoundError:
                    logging.warning(f"File {file} not found")


def main():
    parser = argparse.ArgumentParser(description="Collect function call statistics in python files")
    parser.add_argument("src", type=Path, help="Directory to traverse")
    args = parser.parse_args()
    logging.basicConfig(
        format="%(asctime)s %(levelname)-8s %(message)s",
        datefmt="%H:%M:%S",
        stream=sys.stdout,
        level=logging.INFO,
    )
    parser = get_python_parser()
    stats = FunctionStats()
    process_directory(parser, args.src, stats)
    stats.print_stats()


if __name__ == "__main__":
    main()
