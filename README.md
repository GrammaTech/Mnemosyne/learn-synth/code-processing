# Function Collector

Collect function calls statistics in python files.

## Usage

Create virtual evironment and activate it:

```
tox -e venv
. venv/bin/activate
```

Generate the python parser:
```
git clone https://github.com/tree-sitter/tree-sitter-python
python src/funtion_collector/compile_parser.py
```

Parse python files in a directory
```
collect_functions DIRECTORY
```
