#!/usr/bin/env python

from setuptools import find_packages, setup

setup(
    name="function_collector",
    version="0.4.2",
    description=("Study function calls"),
    author="GrammaTech",
    packages=find_packages("src"),
    package_dir={"": "src"},
    setup_requires=["wheel"],
    zip_safe=False,
    install_requires=["tree_sitter", "tqdm~=4.48"],
    extras_require={
        "test": [
            "flake8~=3.7",
            "isort~=4.3",
            "pytest~=5.4",
            "pytest-cov~=2.10",
            "tox~=3.19",
            "tox-wheel~=0.5.0",
            "pre-commit~=2.6",
            "pytest-mock>=3.0.0",
        ]
    },
    entry_points={
        "console_scripts": ["collect_functions=function_collector.collect:main"]
    },
)
